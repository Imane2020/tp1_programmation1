class Formation{
    constructor(id,nom, description, cout, type){
        this.id = id;
        this.nom = nom;
        this.description = description; 
        this.cout = cout; 
        this.type = type;
    }
}
const data = [   
    new Formation( 0, "Formation Mango DB 1","Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, corporis.", 25, "mongo"),
    new Formation( 1, "Formation Nodejs 2","Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, corporis.", 40, "node"),
    new Formation( 2, "Formation Mango DB 3","Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, corporis.", 55, "mongo"), 
    new Formation( 3, "Formation NodeJs 4","Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, corporis.", 100, "node"),
    new Formation( 4, "Formation Mango DB 5","Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque, corporis.", 85, "mongo"),
];

exports.getFormationById = function(id){
    return data[id]; 
}

exports.getFormationByType = function(type){
    var formation = [];
    for (let i = 0; i < data.length; i++){
        if (data.type == type){
            formation.push(data);
        }
    }
    return formation;
}

exports.allFormation = data;
