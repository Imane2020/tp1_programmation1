class contact{
    constructor(id, nom, prenom, courriel, tel, sujet, demande){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom; 
        this.courriel = courriel;
        this.tel = tel;
        this.sujet = sujet;
        this.demande = demande;
    }
}

const data = [];

exports.addDemande = function(dat){
    let id = data.length;
    data.push(new contact(id, dat.nom, dat.prenom, dat.courriel, dat.tel, dat.sujet, dat.demande))
    return true;
}
