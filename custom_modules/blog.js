class Articles{
    constructor(id,titre, auteur, desc){
        this.id = id;
        this.titre = titre;
        this.auteur = auteur; 
        this.desc = desc;
    }
}

const data = [   
    new Articles( 0, "Article1","Yannick","Lorem ipsum de la descriptiopn 1 "),
    new Articles( 1, "Article2","Clement","Lorem ipsum de la descriptiopn 2 "),
    new Articles( 2, "Article3","David","Lorem ipsum de la descriptiopn 3  "), 
    new Articles( 3, "Article4","Denis","Lorem ipsum de la descriptiopn 4 "),
    new Articles( 4, "Article5","Christian","Lorem ipsum de la descriptiopn 5 "),
    new Articles( 5, "Article6","Martin","Lorem ipsum de la descriptiopn 6 "),
    new Articles( 6, "Article7","Richard","Lorem ipsum de la descriptiopn 7 "),
    new Articles( 7, "Article8","Caroline","Lorem ipsum de la descriptiopn 8"),
    new Articles( 7, "Article9","Caroline","Lorem ipsum de la descriptiopn 9"),
    new Articles( 7, "Article10","Caroline","Lorem ipsum de la descriptiopn 10"),
];

exports.getArticlesById = function(id){
    return data[id]; 
}

exports.allArticles = data;





