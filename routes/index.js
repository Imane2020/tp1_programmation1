var express = require('express');
var router = express.Router();
let Articles = require("../custom_modules/blog.js")
let formations = require("../custom_modules/formations.js")
let demandes = require("../custom_modules/contact.js")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'TP1 - Programmation Node.js', 
    description: 'Bienvenue sur la page de mon premier TP Node.js' });
});

// PAGE BLOG
router.get('/blog', function(req, res, next){
  res.render('blog',{ 
    titre: 'Blog', 
    description: "Découvrez notre blog de la session de printemps", 
    Articles: Articles.allArticles} )
  console.log(Articles)
})
router.get('/blog/:id',function(req,res,next){
  res.render('blog_article',{
    blog_article:Articles.getArticlesById(req.params.id)})
})


//PAGE FORMATION
router.get('/formations', function(req, res, next){
  res.render('formations/index',{ 
    titre: 'Formations par type', 
    description: "Ici toutes nos formations disponibles", 
    formations : formations.allFormation
  })
  // console.log(formations)
})

// Page qui affiche les détails de la formation
router.get('/formations/:id',function(req,res,next){
  res.render('formations/formation_detail',{
    formation_detail:formations.getFormationById(req.params.id)})
})

// page qui affiche les formations de type mongo
router.get('/formations/:type', function(req, res, next){
  res.render('/formations/formations_type/formations_mongoDB',{ 
      titre: 'Liste des formations Mongo DB', 
      description: 'Ici vous trouvez la liste de toutes nos formations Mongo DB',
      formations_mongoDB: formations.getFormationByType('mongo')
  })
})
// page qui affiche les formations de type node
router.get('/formations/:type', function(req, res, next){
  res.render('/formations/formations_type/formations_nodejs',{ 
      titre: 'Liste des formations Mongo DB', 
      description: 'Ici vous trouvez la liste de toutes nos formations Mongo DB',
      formations_mongoDB: formations.getFormationByType('node')
  })
})


// PAGE CONTACT
router.get('/contact/add', function(req, res, next){
  
    res.render('contact/add',{ 
        titre: 'Contactez Nous', 
        description: 'Vous pouvez nous envoyer votre demande en remplissant ce formulaire'} )
    
})

router.post('/contact/add', function(req,res,next){
  let demande = demandes.addDemande(req.body)
  console.log(req.body)
  if(demande){
    res.render('contact/add',{ 
      title: 'La liste', 
      description2: "Votre message a bien été envoyé, pour le consulter, veuillez vous rendre dans le terminal du VSCode" } )
  }
})

module.exports = router;
